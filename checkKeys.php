<?php
/**
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020 Denis Chenu <http://www.sondages.pro>
 * @license MIT
 * @version 0.1.0
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
class checkKeys extends PluginBase
{
    static protected $description = 'Check if crypt key are different in web and cli.';
    static protected $name = 'checkKeys';

    protected $settings = array(
        'keyInfos' => array(
            'type' => 'info',
            'content'=> '',
        ),
    );

    /**
     * Show keys in plugin config
     * @see PluginBase
     */
    public function getPluginSettings($getValues=true)
    {
        $pluginSettings = parent::getPluginSettings($getValues);
        $content = CHTml::tag("div",array("class"=>"h5"),"Current security key");
        $content .= "<ul>";
        $content .=  CHTml::tag("li",array(),sprintf("encryptionkeypair : <code>%s</code>",Yii::app()->getConfig("encryptionkeypair")));
        $content .=  CHTml::tag("li",array(),sprintf("encryptionpublickey : <code>%s</code>",Yii::app()->getConfig("encryptionpublickey")));
        $content .=  CHTml::tag("li",array(),sprintf("encryptionsecretkey : <code>%s</code>",Yii::app()->getConfig("encryptionsecretkey")));
        $content .= "</ul>";
        $content .= "<p>You can check with <code>php yourlimesurveydir/application/commands/console.php plugin --target=checkKeys</code></p>";
        $content .= "<p><strong class='text-danger'>Remind to deactivate this plugin after test</strong></p>";
        $pluginSettings['keyInfos']['content'] = "<div class='well'>".$content."</div>";
        return $pluginSettings;
    }
    public function init()
    {
        /* Menu maganement */
        $this->subscribe('direct','checkKeysCli');
    }

    public function checkKeysCli()
    {
        if($this->event->get("target") != get_class()) {
            return;
        }
        echo "keys before any fix:\n";
        echo "\t encryptionkeypair : ".Yii::app()->getConfig("encryptionkeypair")."\n";
        echo "\t encryptionpublickey : ".Yii::app()->getConfig("encryptionpublickey")."\n";
        echo "\t encryptionsecretkey : ".Yii::app()->getConfig("encryptionsecretkey")."\n";

        $this->fixLsCommand();
        echo "keys after basic fix (sendMailCron 4.0.2):\n";
        echo "\t encryptionkeypair : ".Yii::app()->getConfig("encryptionkeypair")."\n";
        echo "\t encryptionpublickey : ".Yii::app()->getConfig("encryptionpublickey")."\n";
        echo "\t encryptionsecretkey : ".Yii::app()->getConfig("encryptionsecretkey")."\n";

        $this->addSecurityInConfig();
        echo "keys after new security fix:\n";
        echo "\t encryptionkeypair : ".Yii::app()->getConfig("encryptionkeypair")."\n";
        echo "\t encryptionpublickey : ".Yii::app()->getConfig("encryptionpublickey")."\n";
        echo "\t encryptionsecretkey : ".Yii::app()->getConfig("encryptionsecretkey")."\n";
    }


    /**
     * Fix LimeSurvey command function
     * Copy/pasted frrm sendMailCron
     */
    private function fixLsCommand()
    {
        /* Potential bad autoloading in command */
        include_once(dirname(__FILE__)."/DbStorage.php");
        // These take care of dynamically creating a class for each token / response table.
        if (!class_exists('ClassFactory', false)) {
            Yii::import('application.helpers.ClassFactory');
            ClassFactory::registerClass('Token_', 'Token');
            ClassFactory::registerClass('Response_', 'Response');
        }
        if(!function_exists('gT')) {
            Yii::import('application.helpers.common_helper', true);
        }

        $defaulttheme = Yii::app()->getConfig('defaulttheme');
        /* Bad config set for rootdir */
        if( !is_dir(Yii::app()->getConfig('standardthemerootdir').DIRECTORY_SEPARATOR.$defaulttheme) && !is_dir(Yii::app()->getConfig('userthemerootdir').DIRECTORY_SEPARATOR.$defaulttheme)) {
            /* This included can be broken */
            $webroot = (string) Yii::getPathOfAlias('webroot');
            /* Switch according to LimeSurvey version */
            $configFixed = array(
                'standardthemerootdir'   => $webroot.DIRECTORY_SEPARATOR."templates",
                'userthemerootdir'       => $webroot.DIRECTORY_SEPARATOR."upload".DIRECTORY_SEPARATOR."templates",
            );
            if(intval(Yii::app()->getConfig('versionnumber')) > 2) {
                $configFixed = array(
                    'standardthemerootdir'   => $webroot.DIRECTORY_SEPARATOR."themes".DIRECTORY_SEPARATOR."survey",
                    'userthemerootdir'       => $webroot.DIRECTORY_SEPARATOR."upload".DIRECTORY_SEPARATOR."themes".DIRECTORY_SEPARATOR."survey",
                );
            }
            $configConfig = require (Yii::getPathOfAlias('application').DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'config.php');
            if (isset($configConfig['config'])) {
                $configFixed = array_merge($configFixed, $configConfig['config']);
            }
            $aConfigToFix = array(
                'standardthemerootdir',
                'userthemerootdir',
            );
            foreach($aConfigToFix as $configToFix) {
                Yii::app()->setConfig($configToFix,$configFixed[$configToFix]);
            }
        }
    }

    /**
     * add securitu in config
     */
    private function addSecurityInConfig()
    {
        if(intval(Yii::app()->getConfig('versionnumber')) <4) {
            return;
        }
        $aSecurityConfig = require (Yii::getPathOfAlias('application').DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'security.php');
        if(!is_array($aSecurityConfig)) {
            $configDir = Yii::getPathOfAlias('application').DIRECTORY_SEPARATOR.'config';
            throw new CException("Unable to get a valid security.php file in {$configDir}.");
        }
        foreach($aSecurityConfig as $key => $value) {
            Yii::app()->setConfig($key,$value);
        }
    }
}
